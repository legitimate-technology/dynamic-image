<?php

$r = $_GET['r'];
if (!$r || strlen($r) == 0 || $r == "") {

    $r = "cash";
}

$r = trim($r);
$imageURL = "";

if ($r != "cash") {
    // check if image exists
    $file = "/var/www/html/cashout-referral/public/img/referrals/$r.png";

    if (file_exists($file)) {

        $imageURL = "https://legitimate.ams3.digitaloceanspaces.com/cashout-agents/$r.png";
    }
}

if (strlen($imageURL) < 10) {

    $imageURL = "https://legitimate.ams3.digitaloceanspaces.com/cashout-agents/cash.png";

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta property="og:title" content="Bonga Points to Cash">
    <meta property="og:description"
          content="Instantly Convert your Bonga Points to Cash with Ease. Minimum conversion amount is 100Bonga points">
    <meta property="og:image" content="<?= $imageURL ?>">
    <meta property="og:url" content="https://cashout.smestech.co.ke">

    <meta property="og:site_name" content="Bonga Points to Cash"/>
    <meta property="og:url" content="https://cashout.smestech.co.ke"/>
    <meta property="og:type" content="website"/>
    <meta name="title" property="og:title" content="Bonga Points to Cash"/>
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1024">
    <meta property="og:image:height" content="512">
    <meta name="description" property="og:description"
          content="Instantly Convert your Bonga Points to Cash with Ease. Minimum conversion amount is 100Bonga points"/>
    <meta name="twitter:card" content="Bonga Points to Cash"/>
    <meta name="twitter:site" description="@smestech"/>
    <meta name="twitter:title" content="Bonga Points to Cash"/>
    <meta name="twitter:description"
          content="Instantly Convert your Bonga Points to Cash with Ease. Minimum conversion amount is 100Bonga points"/>
    <meta name="twitter:image:src" content="<?= $imageURL ?>">
    <meta name="twitter:domain" content="https://cashout.smestech.co.ke">

    <title>Bonga Points to Cash</title>

</head>
<body>

<p>Welcome to our awesome site</p>

</body>
</html>